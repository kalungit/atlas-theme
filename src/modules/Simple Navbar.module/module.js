(function () {
  const navbar = document.querySelector('.kl-navbar');
  const hamburger = document.querySelector('.kl-navbar__hamburger');

  if (hamburger) {
    hamburger.addEventListener('click', function () {
      hamburger.classList.toggle('is-active');
      navbar.classList.toggle('kl-navbar--active');
    });
  }

  const ACTIVE_CLASS = 'kl-navbar__parent--active';

  const selected = [];
  const nodes = document.querySelectorAll('.kl-navbar__parent');

  nodes.forEach(function (node) {
    function handleClick(event) {
      event.stopPropagation();

      if (selected.length <= 0) {
        selected.push(node);
        node.classList.add(ACTIVE_CLASS);
        return;
      }

      do {
        const last = selected.pop();

        if (last === node) {
          last.classList.remove(ACTIVE_CLASS);
          break;
        } else if (last.contains(node)) {
          selected.push(last);
          selected.push(node);
          node.classList.add(ACTIVE_CLASS);
          break;
        } else {
          last.classList.remove(ACTIVE_CLASS);

          if (selected.length === 0) {
            selected.push(node);
            node.classList.add(ACTIVE_CLASS);
            break;
          }
        }
      } while (selected.length > 0);
    }

    node.addEventListener('click', handleClick);
  });
})();
